import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  readonly BaseURI: string = 'http://localhost:8080';

  constructor(private http: HttpClient) { }

  getUsers(): Observable<any> {
    return this.http.get<any>(this.BaseURI + '/users');
  }

  getUsersSortedBySuccess(): Observable<any> {
    return this.http.get<any>(this.BaseURI + '/users?sort=numberOfWonCompetitions,desc');
  }

  getUsersSortedByActivity(): Observable<any> {
    return this.http.get<any>(this.BaseURI + '/users?sort=numberOfCompetedCompetitions,desc');
  }

  getUsersSortedByRatio(): Observable<any> {
    return this.http.get<any>(this.BaseURI + '/users?sort=ratioOfWinLoss,desc');
  }

  getUser(id: string | undefined): Observable<any> {
    return this.http.get<any>(this.BaseURI + '/users/' + id + '?projection=details');
  }
  getUserCompetitionsByUserId(id: string | undefined): Observable<any> {
    return this.http.get<any>(this.BaseURI + '/users/' + id + '/userCompetitions');
  }
  postUser(user: User) {
    return this.http.post<User>(this.BaseURI + '/users', user);

  }
  patchUser(userId: any, body: any) {
    return this.http.patch<User>(this.BaseURI + `/users/${userId}`, body);

  }


  filterUsers(filterCountry: string, filterFirstName: string, filterLastName: string) {
    return this.http.get<any>(this.BaseURI + `/users/search/`+
      `findByUserAddressStateIgnoreCaseContainingAndFirstNameIgnoreCaseContainingAndLastNameIgnoreCaseContaining`+
      `?state=${filterCountry?filterCountry:''}&name=${filterFirstName?filterFirstName:''}&surname=${filterLastName?filterLastName:''}`);

  }
}
