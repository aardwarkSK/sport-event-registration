import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from '../models/category.model';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  readonly BaseURI: string = 'http://localhost:8080';

  constructor(private http: HttpClient) { }

  getCategories(): Observable<any> {
    return this.http.get<any>(this.BaseURI + '/category');
  }

  getCategory(id: string) {
    return this.http.get(this.BaseURI + '/category/' + id);
  }

  postCategory(category: Category) {
    return this.http.post(this.BaseURI + '/category', category);

  }
}
