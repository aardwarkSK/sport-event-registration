import { HttpClient } from '@angular/common/http';
import { stringify } from '@angular/compiler/src/util';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Competition } from '../models/competition.model';

@Injectable({
  providedIn: 'root'
})
export class CompetitionService {
  readonly BaseURI: string = 'http://localhost:8080';

  constructor(private http: HttpClient) { }

  getCompetitions(): Observable<any> {
    return this.http.get<any>(this.BaseURI + '/competitions');
  }
  getCompetitionsSortByLatestPrevious() {
    return this.http.get<any>(this.BaseURI + '/competitions/search/findCompetitionByIsFinished?finished=true&sort=numberOfMembers,desc');
  }

  getCompetitionsSortByUpcoming() {
    return this.http.get<any>(this.BaseURI + '/competitions/search/findCompetitionByIsFinished?finished=false&sort=dateOfCompetition,asc');
  }

  getCompetitionsSortByMembers() {
    return this.http.get<any>(this.BaseURI + '/competitions?sort=numberOfMembers,desc');
  }

  getCompetition(id: string): Observable<any> {
    return this.http.get<any>(this.BaseURI + '/competitions/' + id + '');
  }

  postCompetition(competition: Competition) {
    return this.http.post(this.BaseURI + '/competitions', competition);

  }

  deleteUserCompetition(id: number) {
    return this.http.delete<any>(this.BaseURI + '/userCompetitions/' + id);
  }

  postUserCompetition(body: any): Observable<any> {
    const bodyWithLinks = {
      competition: this.BaseURI + "/competitions/" + body.competitionId,
      user: this.BaseURI + "/users/" + body.userId,
      placed: body.placed ? body.placed : 0,
      category: body.categoryId ? this.BaseURI + "/categories/" + body.categoryId : undefined
    }
    return this.http.post<any>(this.BaseURI + '/userCompetitions', bodyWithLinks);
  }

  patchUserCompetition(id: number, body: any): Observable<any> {
    return this.http.patch<any>(this.BaseURI + `/userCompetitions/${id}`, body);
  }

  filterCompetitions(filterName: string, filterState: string, filterFinished: boolean) {
    return this.http.get<any>(this.BaseURI + `/competitions/search/findByCompetitionAddress`+
      `StateIgnoreCaseContainingAndNameIgnoreCaseContainingAndIsFinished?state=`+
    `${filterState?filterState:''}&name=${filterName?filterName:''}&finished=${filterFinished}`);
  }
}
