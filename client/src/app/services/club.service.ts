import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Club } from '../models/club.model';

@Injectable({
  providedIn: 'root'
})
export class ClubService {
  readonly BaseURI: string = 'http://localhost:8080';

  constructor(private http: HttpClient) { }

  getClubs(): Observable<any> {
    return this.http.get<any>(this.BaseURI + '/clubs');
  }

  getClubsSortByMembers(): Observable<any> {
    return this.http.get<any>(this.BaseURI + '/clubs?sort=numberOfMembers,desc');
  }
  getClubsSortBySuccess(): Observable<any> {
    return this.http.get<any>(this.BaseURI + '/clubs?sort=numberOfMembers,desc');
  }
  getClubsSortByActivity(): Observable<any> {
    return this.http.get<any>(this.BaseURI + '/clubs?sort=numberOfMembers,desc');
  }

  getClub(id: string): Observable<any> {
    return this.http.get<any>(this.BaseURI + '/clubs/' + id);
  }

  postClub(club: Club) {
    return this.http.post(this.BaseURI + '/clubs', club);
  }


  filterClubs(filterName: string, filterState: string) : Observable<any>{
    return this.http.get<any>(this.BaseURI
      + `/clubs/search/findByClubAddressStateIgnoreCaseContainingAndNameIgnoreCaseContaining?state=${filterState? filterState: ''}&name=${filterName? filterName: ''}` );

  }
}
