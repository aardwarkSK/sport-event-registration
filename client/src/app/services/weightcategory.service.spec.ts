import { TestBed } from '@angular/core/testing';

import { WeightcategoryService } from './weightcategory.service';

describe('WeightcategoryService', () => {
  let service: WeightcategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WeightcategoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
