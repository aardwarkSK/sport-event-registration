import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProfileComponent } from './components/user/profile/profile.component';
import { MaterialModule } from './material.module';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CompetitionsListComponent } from './components/competitions-list/competitions-list.component';
import { CompetitionInfoComponent } from './components/competition-info/competition-info.component';
import { ClubsListComponent } from './components/clubs-list/clubs-list.component';
import { ClubInfoComponent } from './components/club-info/club-info.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { UserInfoComponent } from './components/user-info/user-info.component';
import { HttpClientModule } from '@angular/common/http';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { FinishRegistrationComponent } from './components/finish-registration/finish-registration.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgProgressModule } from 'ngx-progressbar';
import { NgProgressHttpModule } from 'ngx-progressbar/http';
import { CreateClubComponent } from './components/create-club/create-club.component';
import { CreateCompetitionComponent } from './components/create-competition/create-competition.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatSelectCountryModule } from "@angular-material-extensions/select-country";
import { TableModule } from 'primeng/table';
import { FieldPipe } from './pipes/field.pipe';
import { InputTextModule } from 'primeng/inputtext';
import { InputNumber } from 'primeng/inputnumber';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { UserCompetitionDialogComponent } from './components/competition-info/user-competition-dialog/user-competition-dialog.component';
import { DropdownItem, DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { DialogModule, Dialog } from 'primeng/dialog'
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { RegisterForCompDialogComponent } from './components/competition-info/register-for-comp-dialog/register-for-comp-dialog.component';
import { MenubarModule } from 'primeng/menubar';
import { SplitButtonModule } from 'primeng/splitbutton';
import {RippleModule} from "primeng/ripple";
import {OrderListModule} from "primeng/orderlist";
import {CardModule} from "primeng/card";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {CheckboxModule} from "primeng/checkbox";
import {RegisterClubMembersForCompDialogComponent} from "./components/competition-info/register-club-members-for-comp-dialog/register-club-members-for-comp-dialog.component";
import {CascadeSelectModule} from "primeng/cascadeselect";





function initializeKeycloak(keycloak: KeycloakService): () => Promise<boolean> {
  return () =>
    keycloak.init({
      config: {
        url: 'http://localhost:8180/auth',
        realm: 'oauth2-judo-registration',
        clientId: 'oauth2-judo-registration',
      },
      initOptions: {
        onLoad: 'check-sso',
        silentCheckSsoRedirectUri:
          window.location.origin + '/assets/silent-check-sso.html',
      },
    });
}

@NgModule({
  declarations: [
    FieldPipe,
    AppComponent,
    ProfileComponent,
    HomeComponent,
    NavbarComponent,
    CompetitionsListComponent,
    CompetitionInfoComponent,
    ClubsListComponent,
    ClubInfoComponent,
    UsersListComponent,
    UserInfoComponent,
    FinishRegistrationComponent,
    CreateClubComponent,
    CreateCompetitionComponent,
    UserCompetitionDialogComponent,
    RegisterForCompDialogComponent,
    RegisterClubMembersForCompDialogComponent

  ],
  imports: [
    SplitButtonModule,
    MenubarModule,
    ConfirmDialogModule,
    DialogModule,
    ButtonModule,
    DropdownModule,
    DynamicDialogModule,
    InputTextModule,
    TableModule,
    MatSelectCountryModule.forRoot('en'),
    FontAwesomeModule,
    NgProgressModule,
    NgProgressHttpModule,
    KeycloakAngularModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    RippleModule,
    OrderListModule,
    CardModule,
    ProgressSpinnerModule,
    CheckboxModule,
    CascadeSelectModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
