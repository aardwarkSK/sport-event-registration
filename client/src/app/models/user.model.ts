import { Adress } from "./adress.model";
import { Category } from "./category.model";
import { Club } from "./club.model";
import { Competition } from "./competition.model";
import { Role } from "./role.model";

export class User {
    id!: string;
    firstName!: string;
    lastName!: string;
    phone!: string;
    email!: string;
    imgUrl!: string;
    category!: any;
    numberOfWonCompetitions!: number;
    numberOfCompetedCompetitions!: number;
    ratioOfWinLoss!: number;
    belt!: string;
    userAddress!: Adress;
    club!: any;
    ownedClub!: any;
    club_id!: number | null;
    competingCompetitions!: Competition[];
    roles!: Role[];
    organizedCompetitions!: Competition;
    dateOfBirth!: Date;
    _links!: any;

    constructor() {
        this.userAddress = new Adress();
    }
}