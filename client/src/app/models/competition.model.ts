import { Adress } from "./adress.model";

export class Competition {
    id!: number;
    name!: string;
    about!: string;
    organizer!: any;
    mail!: string;
    phone!: string;
    isInternational!: boolean;
    dateOfCompetition!: Date;
    dateAdded!: Date;
    competitionAddress!: Adress;
    _links!: any;
    numberOfMembers!: number;
    userCompetitions!: any;
    _embedded: any;

    constructor() {
        this.competitionAddress = new Adress();
        this.dateAdded = new Date();
    }

}