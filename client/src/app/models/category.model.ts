export class Category {

    id!: number;
    weightUnder!: number;
    ageUnder!: number;
    sex!: { id: number, name: string };
    _links!: any;
}