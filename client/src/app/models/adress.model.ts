import { Club } from "./club.model";
import { Competition } from "./competition.model";
import { User } from "./user.model";

export class Adress {
    id!: number;
    street!: string;
    city!: string;
    state!: string;
    zipCode!: string;
    alpha2Code!: string;
    club!: Club;
    user!: User;
    competition!: Competition;
}
