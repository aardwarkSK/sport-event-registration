import { Adress } from "./adress.model";
import { User } from "./user.model";

export class Club {
    id!: number;
    name!: string;
    about!: string;
    mail!: string;
    phone!: string;
    clubAddress!: Adress;
    members!: User[];
    _links!: any;
    owner!: any;
    _embedded!: any;

    constructor() {
        this.clubAddress = new Adress();
    }

}