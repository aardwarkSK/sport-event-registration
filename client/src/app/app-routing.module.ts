import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClubInfoComponent } from './components/club-info/club-info.component';
import { ClubsListComponent } from './components/clubs-list/clubs-list.component';
import { CompetitionInfoComponent } from './components/competition-info/competition-info.component';
import { CompetitionsListComponent } from './components/competitions-list/competitions-list.component';
import { CreateClubComponent } from './components/create-club/create-club.component';
import { CreateCompetitionComponent } from './components/create-competition/create-competition.component';
import { FinishRegistrationComponent } from './components/finish-registration/finish-registration.component';
import { HomeComponent } from './components/home/home.component';
import { UserInfoComponent } from './components/user-info/user-info.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { AuthGuard } from './core/auth.guard';

const routes: Routes = [
  {
    path: '', redirectTo: '/home', pathMatch: 'full'
  },
  {
    path: 'home', component: HomeComponent,
  },
  {
    path: 'competitors', component: UsersListComponent
  },
  {
    path: 'clubs', component: ClubsListComponent
  },
  {
    path: 'competitions', component: CompetitionsListComponent
  },
  {
    path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]
  },
  {
    path: 'finish-registration', component: FinishRegistrationComponent, canActivate: [AuthGuard]
  },
  {
    path: 'create-club', component: CreateClubComponent, canActivate: [AuthGuard]
  },
  {
    path: 'create-competition', component: CreateCompetitionComponent, canActivate: [AuthGuard]
  },
  { path: 'user-detail/:id', component: UserInfoComponent },
  { path: 'competition-detail/:id', component: CompetitionInfoComponent },
  { path: 'club-detail/:id', component: ClubInfoComponent },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
