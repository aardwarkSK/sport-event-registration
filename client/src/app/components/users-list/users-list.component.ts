import {Component, OnInit} from '@angular/core';
import {User} from 'src/app/models/user.model';
import {UserService} from 'src/app/services/user.service';
import {Observable, forkJoin} from 'rxjs';
import {Router} from '@angular/router';


@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  mostSuccessfulUsers!: User[];
  mostActiveUsers!: User[];
  bestRatioUsers!: User[];
  filterDisplayed = false;
  filterFirstName!: string;
  filterLastName!: string;
  filterCountry!: string;
   filteredUsers: User[] = [];


  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit(): void {
    this.getInitialData();
  }

  getInitialData() {
    forkJoin([
      this.userService.getUsersSortedBySuccess(),
      this.userService.getUsersSortedByActivity(),
      this.userService.getUsersSortedByRatio()])
      .subscribe(results => {
        this.mostSuccessfulUsers = results[0]._embedded.users;
        this.mostActiveUsers = results[1]._embedded.users;
        this.bestRatioUsers = results[2]._embedded.users;
      });
  }

  openDetails(id: string) {
    this.router.navigate(['/user-detail', id]);
  }

  onCountrySelected($event: any) {
    this.filterCountry = $event.name;
  }

  searchUsers() {
    this.userService.filterUsers(this.filterCountry,this.filterFirstName, this.filterLastName)
      .subscribe((r:any)=>{this.filteredUsers = r._embedded.users});
  }

  turnOffFilter() {
    this.filterDisplayed = false;
    this.filteredUsers = [];
    this.getInitialData()
  }
}
