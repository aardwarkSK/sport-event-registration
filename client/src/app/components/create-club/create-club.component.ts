import { Component, OnInit } from '@angular/core';
import { Adress } from 'src/app/models/adress.model';
import { Club } from 'src/app/models/club.model';
import { ClubService } from 'src/app/services/club.service';
import { Location } from '@angular/common';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { KeycloakService } from 'keycloak-angular';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-create-club',
  templateUrl: './create-club.component.html',
  styleUrls: ['./create-club.component.scss']
})
export class CreateClubComponent implements OnInit {

  club: Club = new Club();
  countryFormControl = new FormControl();
  countryFormGroup!: FormGroup;
  ownerReference!: string;
  alreadyOwnsClub: boolean = true;

  constructor(private keycloakService: KeycloakService,
    private clubService: ClubService,
    public location: Location,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private userService: UserService) {
  }

  ngOnInit(): void {
    this.getOwnerReference();
  }

  createClub() {
    this.clubService.postClub(this.club).subscribe(c => {
      this.openSuccessSnackBar("Club created!");
      this.alreadyOwnsClub = true;
    });
  }

  onCountrySelected($event: any) {
    this.club.clubAddress.state = $event.name;
    this.club.clubAddress.alpha2Code = $event.alpha2Code;
  }

  async getOwnerReference() {
    if (await this.keycloakService.isLoggedIn()) {
      const authDetails = await this.keycloakService.loadUserProfile();
      this.userService.getUser(authDetails.id).subscribe(user => {
        this.ownerReference = user._links.self.href;
        this.club.owner = this.ownerReference;
        this.alreadyOwnsClub = !!user.ownedClub;
      });
    }
  }

  openSuccessSnackBar(message: string) {
    this._snackBar.open(message, '', { duration: 2000 });
  }
}
