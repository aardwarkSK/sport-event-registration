import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';
import { KeycloakLoginOptions } from 'keycloak-js';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  isShown = false;
  userDetails: any;
  keycloakLoginOptions: KeycloakLoginOptions = {
    redirectUri: 'http://localhost:4200/finish-registration'
  }
  items!: MenuItem[];

  constructor(private keycloakService: KeycloakService, public router: Router) { }

  ngOnInit(): void {
    this.isLoggedIn();
    this.fillMenuItems();
  }

  async isLoggedIn() {
    if (await this.keycloakService.isLoggedIn()) {
      this.userDetails = await this.keycloakService.loadUserProfile();
    }
  }

  logOut(): void {
    const onLogoutRedirectUrl = 'http://localhost:4200/home';
    this.keycloakService.logout(onLogoutRedirectUrl).then(() => this.keycloakService.clearToken());
    ;
  }

  logIn() {
    this.keycloakService.login(this.keycloakLoginOptions);
  }

  fillMenuItems() {
    this.items = [
      {
        label: 'Competitions',
        icon: 'pi pi-fw pi-calendar',
        routerLink: ["/competitions"],
        routerLinkActiveOptions: { exact: true }
      },
      {
        label: 'Clubs',
        icon: 'pi pi-fw pi-users',
        routerLink: ["/clubs"],
        routerLinkActiveOptions: { exact: true }
      },
      {
        label: 'Competitors',
        icon: 'pi pi-fw pi-user',
        routerLink: ["/competitors"],
        routerLinkActiveOptions: { exact: true }
      },
    ];


  }

}
