import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { CategoryService } from 'src/app/services/category.service';
import { CompetitionService } from 'src/app/services/competition.service';
import { faWeight, faVenusMars, faHourglass } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-register-for-comp-dialog',
  templateUrl: './register-for-comp-dialog.component.html',
  styleUrls: ['./register-for-comp-dialog.component.scss']
})
export class RegisterForCompDialogComponent implements OnInit {
  faWeight = faWeight
  faVenusMars = faVenusMars
  faHourGlass = faHourglass
  categories!: any[];
  selectedCategory: any;
  clonedData: any;

  constructor(public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private categoryService: CategoryService,
    private competitionService: CompetitionService) { }

  ngOnInit(): void {
    this.clonedData = Object.assign({}, this.config.data);
    this.getCategories();
  }

  getCategories() {
    this.categoryService.getCategories().subscribe(c => {
      this.categories = c._embedded.category;
      this.selectedCategory = c._embedded.category.find((c: any) => c.id === this.clonedData.user.category.id);
    })
  }

  save() {
    this.competitionService.postUserCompetition({ competitionId: this.clonedData.competition.id, userId: this.clonedData.user.id, categoryId: this.selectedCategory.id })
      .subscribe(uc => { this.ref.close(true) }
        ,
        err => { this.ref.close(false); }
      );
  }
}
