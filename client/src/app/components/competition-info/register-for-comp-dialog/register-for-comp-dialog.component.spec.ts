import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterForCompDialogComponent } from './register-for-comp-dialog.component';

describe('RegisterForCompDialogComponent', () => {
  let component: RegisterForCompDialogComponent;
  let fixture: ComponentFixture<RegisterForCompDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterForCompDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterForCompDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
