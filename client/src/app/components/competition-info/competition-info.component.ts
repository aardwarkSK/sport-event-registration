import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Table } from 'primeng/table';
import { Subscription } from 'rxjs';
import { Competition } from 'src/app/models/competition.model';
import { DialogService } from 'primeng/dynamicdialog';
import { CompetitionService } from 'src/app/services/competition.service';
import { UserCompetitionDialogComponent } from './user-competition-dialog/user-competition-dialog.component';
import { ConfirmationService } from 'primeng/api';
import { KeycloakService } from 'keycloak-angular';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RegisterForCompDialogComponent } from './register-for-comp-dialog/register-for-comp-dialog.component';
import {RegisterClubMembersForCompDialogComponent} from "./register-club-members-for-comp-dialog/register-club-members-for-comp-dialog.component";


@Component({
  selector: 'app-competition-info',
  templateUrl: './competition-info.component.html',
  styleUrls: ['./competition-info.component.scss'],
  providers: [DialogService, ConfirmationService]
})
export class CompetitionInfoComponent implements OnInit, OnDestroy {
  @ViewChild('dt') table!: Table;
  private routeSub!: Subscription;
  competition!: Competition;
  userId: any;
  user!: User;
  isSignedUp = false;
  selectedComp!: any[];


  constructor(private confirmationService: ConfirmationService,
    private competitionService: CompetitionService,
    private keycloakService: KeycloakService,
    private route: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private userService: UserService,
    public dialogService: DialogService) { }

  ngOnInit(): void {
    this.getCompetitionFromRoute();
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

  getCompetitionFromRoute() {
    this.routeSub = this.route.params.subscribe(params => {
      const competitionId = params["id"];
      if (competitionId) {
        this.competitionService.getCompetition(competitionId).subscribe(result => {
          this.competition = result;
          this.getUser();
        });
      }
    });
  }

  showDialog(userComp: any) {
    const ref = this.dialogService.open(UserCompetitionDialogComponent, {
      data: userComp
    });

    ref.onClose.subscribe(r => {
      r === true ? this.openSuccessSnackBar("Edited successfully") : '';
      this.getCompetitionFromRoute();
    });
  }

  finishCompetition() {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to finish competition?',
      accept: () => {
      }
    });
  }

  registerForCompetition() {
    const ref = this.dialogService.open(RegisterForCompDialogComponent, {
      data: { user: this.user, competition: this.competition },
    })
    ref.onClose.subscribe(r => {
      const message = (r === true ? "Registered successfully" : "Something gone wrong");
      this.openSuccessSnackBar(message);
      this.getCompetitionFromRoute();
    });
  }

  unregisterFromCompetition() {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to unregister from this competition?',
      accept: () => {
        const userCompToDelete = this.competition._embedded.userCompetitions.find((u: any) => { return u.user.id === this.user.id && u.competition.id === this.competition.id });
        this.competitionService.deleteUserCompetition(userCompToDelete.id).subscribe(u => {
          this.getCompetitionFromRoute();
          this.openSuccessSnackBar("Unregistered successfully.");
        });
      }
    });
  }

  async getUser() {
    if (await this.keycloakService.isLoggedIn()) {
      const user = await this.keycloakService.loadUserProfile();
      this.userId = user.id;
      this.userService.getUser(this.userId).subscribe(u => {this.user = u;});

      this.getIsUserSignedUpForCompetition();
    }
  }

  getIsUserSignedUpForCompetition() {
    if (this.competition._embedded.userCompetitions) {
      this.isSignedUp = this.competition._embedded.userCompetitions.filter((c: any) => c.user.id === this.userId).length > 0;
    } else {
      this.isSignedUp = false;
    }

  }

  openSuccessSnackBar(message: string) {
    this._snackBar.open(message, '', { duration: 2000 });
  }

  signUpMembers() {
    const ref = this.dialogService.open(RegisterClubMembersForCompDialogComponent, {
      data: { club: this.user.ownedClub, competition: this.competition },width: '70%'
    })
    ref.onClose.subscribe(r => {
      console.log(r)
      r === true ? this.openSuccessSnackBar("Members registered successfully"):'';
      this.getCompetitionFromRoute();
    });
  }
}


