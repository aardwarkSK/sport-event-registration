import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { CategoryService } from 'src/app/services/category.service';
import { faWeight, faVenusMars, faHourglass } from '@fortawesome/free-solid-svg-icons';
import { CompetitionService } from 'src/app/services/competition.service';


@Component({
  selector: 'app-user-competition-dialog',
  templateUrl: './user-competition-dialog.component.html',
  styleUrls: ['./user-competition-dialog.component.scss']
})
export class UserCompetitionDialogComponent implements OnInit {
  faWeight = faWeight
  faVenusMars = faVenusMars
  faHourGlass = faHourglass
  categories!: any[];
  selectedPlace!: number
  selectedCategory: any;

  constructor(public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private categoryService: CategoryService,
    private competitionService: CompetitionService) { }

  ngOnInit(): void {
    this.selectedPlace = this.config.data.placed;
    this.getCategories();
  }

  getCategories() {
    this.categoryService.getCategories().subscribe(c => {
      this.categories = c._embedded.category;
      this.selectedCategory = c._embedded.category.find((c: any) => c.id === this.config.data.category.id);
    })
  }

  save() {
    const body = {
      category: this.selectedCategory._links.self.href,
      placed: this.selectedPlace
    }
    this.competitionService.patchUserCompetition(this.config.data.id, body).subscribe(r => {
      this.ref.close(true);
    },
      err => {
        this.ref.close(false);
      }
    );
  }
}
