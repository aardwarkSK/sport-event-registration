import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCompetitionDialogComponent } from './user-competition-dialog.component';

describe('UserCompetitionDialogComponent', () => {
  let component: UserCompetitionDialogComponent;
  let fixture: ComponentFixture<UserCompetitionDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserCompetitionDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCompetitionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
