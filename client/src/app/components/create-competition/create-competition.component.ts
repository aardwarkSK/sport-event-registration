import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Adress } from 'src/app/models/adress.model';
import { Competition } from 'src/app/models/competition.model';
import { CompetitionService } from 'src/app/services/competition.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { KeycloakService } from 'keycloak-angular';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-create-competition',
  templateUrl: './create-competition.component.html',
  styleUrls: ['./create-competition.component.scss']
})
export class CreateCompetitionComponent implements OnInit {
  competition: Competition = new Competition();
  countryFormControl = new FormControl();
  countryFormGroup!: FormGroup;
  ownerReference!: string;

  constructor(private keycloakService: KeycloakService,
    private userService: UserService,
    private _snackBar: MatSnackBar,
    private competitionService: CompetitionService,
    public location: Location,
    private formBuilder: FormBuilder) {

  }

  ngOnInit(): void {
    this.getOwnerReference();
    this.countryFormGroup = this.formBuilder.group({
      country: []
    });
  }

  dateAfterTodayFilter = (d: Date | null): boolean => {
    const day = (d || new Date());
    const today = new Date(new Date().getTime() - 86400000);
    return day >= today;
  };

  createCompetition() {
    this.competitionService.postCompetition(this.competition).subscribe(() => {
      this.openSuccessSnackBar("Competition created!");
    });
  }

  onCountrySelected($event: any) {
    this.competition.competitionAddress.state = $event.name;
    this.competition.competitionAddress.alpha2Code = $event.alpha2Code;
  }

  async getOwnerReference() {
    if (await this.keycloakService.isLoggedIn()) {
      const authDetails = await this.keycloakService.loadUserProfile();
      this.userService.getUser(authDetails.id).subscribe(user => {
        this.ownerReference = user._links.self.href;
        this.competition.organizer = this.ownerReference;
      });
    }
  }

  openSuccessSnackBar(message: string) {
    this._snackBar.open(message, '', { duration: 2000 });
  }



}
