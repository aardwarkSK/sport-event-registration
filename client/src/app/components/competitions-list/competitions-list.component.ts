import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Observable, forkJoin} from 'rxjs';
import {Competition} from 'src/app/models/competition.model';
import {CompetitionService} from 'src/app/services/competition.service';

@Component({
  selector: 'app-competitions-list',
  templateUrl: './competitions-list.component.html',
  styleUrls: ['./competitions-list.component.scss']
})
export class CompetitionsListComponent implements OnInit {
  competitionsSortByMembers!: Competition[];
  competitionsSortByLatestPrevious!: Competition[];
  competitionsSortByUpcoming!: Competition[];
  filterDisplayed = false;
  filterName!: string;
  filterCountry!: string;
  filterFinished: boolean = false;
  filteredCompetitions: Competition[] = [];

  constructor(public router: Router, private competitionService: CompetitionService) {
  }

  ngOnInit(): void {
    this.getInitialData()
  }

  private getInitialData() {
    forkJoin([this.competitionService.getCompetitionsSortByMembers(),
      this.competitionService.getCompetitionsSortByLatestPrevious(),
      this.competitionService.getCompetitionsSortByUpcoming()]).subscribe(results => {
      this.competitionsSortByMembers = results[0]._embedded.competitions;
      this.competitionsSortByLatestPrevious = results[1]._embedded.competitions;
      this.competitionsSortByUpcoming = results[2]._embedded.competitions;
    });
  }

  openDetails(id: number) {
    this.router.navigate(['/competition-detail', id]);
  }

  onCountrySelected($event: any) {
    this.filterCountry = $event.name

  }

  searchCompetitions() {
    this.competitionService.filterCompetitions(this.filterName, this.filterCountry, this.filterFinished)
      .subscribe((r: any) => this.filteredCompetitions = r._embedded.competitions);
  }

  turnOffFilter() {
    this.filteredCompetitions = [];
    this.getInitialData();
  }

}
