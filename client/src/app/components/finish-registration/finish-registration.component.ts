import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';
import { Adress } from 'src/app/models/adress.model';
import { Category } from 'src/app/models/category.model';
import { Club } from 'src/app/models/club.model';
import { User } from 'src/app/models/user.model';
import { CategoryService } from 'src/app/services/category.service';
import { ClubService } from 'src/app/services/club.service';
import { UserService } from 'src/app/services/user.service';
import { Country } from '@angular-material-extensions/select-country';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';



@Component({
  selector: 'app-finish-registration',
  templateUrl: './finish-registration.component.html',
  styleUrls: ['./finish-registration.component.scss']
})
export class FinishRegistrationComponent implements OnInit {

  authDetails: any;
  user: User = new User();
  belts: string[] = ["White", "Yellow", "Orange", "Green", "Blue", "Brown", "Black"];
  clubs!: Club[];
  categories!: Category[];
  isLoading: boolean = true;
  countryFormControl = new FormControl();
  countryFormGroup!: FormGroup;

  constructor(private userService: UserService,
    private categoryService: CategoryService,
    private keycloakService: KeycloakService,
    private clubService: ClubService,
    private router: Router,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder) {
    this.user = new User();
    this.user.userAddress = new Adress();
    this.user.club = new Club();
    this.user.category = new Category();
  }

  ngOnInit(): void {

    this.countryFormGroup = this.formBuilder.group({
      country: []
    });


    this.decideIfProfileFinishingIsNeeded();
    this.getClubs()
    this.getCategories();
  }

  async decideIfProfileFinishingIsNeeded() {
    if (await this.keycloakService.isLoggedIn()) {
      this.authDetails = await this.keycloakService.loadUserProfile();
      this.getIfUserExistsInResourceDb();

    }
  }

  getIfUserExistsInResourceDb() {
    this.userService.getUser(this.authDetails.id).subscribe(userFromResourceDb => {
      this.router.navigateByUrl('/home');
    }, err => {
      this.isLoading = false
      this.fillBasicUserInfoFromAuth();
    });
  }

  getClubs() {
    this.clubService.getClubs().subscribe(response => this.clubs = response._embedded.clubs)
  }
  getCategories() {
    this.categoryService.getCategories().subscribe(response => this.categories = response._embedded.category)
  }

  finishRegistration() {
    this.userService.postUser(this.user).subscribe(() => {
      this.openSuccessSnackBar();
      this.router.navigateByUrl('/home');
    });
  }

  saveUserWithoutDetails() {
    this.fillBasicUserInfoFromAuth();
    this.finishRegistration();


  }

  fillBasicUserInfoFromAuth() {
    this.user = new User();
    this.user.id = this.authDetails.id;
    this.user.email = this.authDetails.email;
    this.user.firstName = this.authDetails.firstName;
    this.user.lastName = this.authDetails.lastName;
    this.user.category = undefined;
    this.user.userAddress = new Adress();
    this.user.club = new Club();
    this.user.category = new Category();
    this.countryFormControl.setValue({
      name: this.user.userAddress.state,
      alpha2Code: '',
      alpha3Code: '',
      numericCode: '',
      callingCode: ''
    })

  }

  openSuccessSnackBar() {
    this._snackBar.open('Registration completed!', '', { duration: 2000 });
  }

  onCountrySelected($event: any) {
    this.user.userAddress.state = $event.name;
    this.user.userAddress.alpha2Code = $event.alpha2Code;
  }
}
