import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {forkJoin} from 'rxjs';
import {Club} from 'src/app/models/club.model';
import {ClubService} from 'src/app/services/club.service';

@Component({
  selector: 'app-clubs-list',
  templateUrl: './clubs-list.component.html',
  styleUrls: ['./clubs-list.component.scss']
})
export class ClubsListComponent implements OnInit {
  clubsSortByMembers!: Club[] | undefined;
  clubsSortBySuccess!: Club[] | undefined;
  clubsSortByActivity!: Club[] | undefined;
  filterDisplayed = false;
  filterState!: string;
  filterName!: string;
  filteredClubs!: Club[] | undefined;

  constructor(public router: Router, private clubService: ClubService) {
  }

  ngOnInit(): void {
    this.getInitialData()
  }

  getInitialData() {
    this.filteredClubs = undefined;
    forkJoin([this.clubService.getClubsSortByMembers(),
      this.clubService.getClubsSortBySuccess(),
      this.clubService.getClubsSortByActivity()]).subscribe(results => {
      this.clubsSortByMembers = results[0]._embedded.clubs;
      this.clubsSortBySuccess = results[1]._embedded.clubs;
      this.clubsSortByActivity = results[2]._embedded.clubs;
    });
  }

  openDetails(id: number) {
    this.router.navigate(['/club-detail', id]);
  }

  searchClubs() {
    this.clubsSortByActivity = undefined;
    this.clubsSortBySuccess = undefined;
    this.clubsSortByMembers = undefined;
    this.clubService.filterClubs(this.filterName, this.filterState).subscribe((r:any) => this.filteredClubs = r._embedded.clubs);
  }

  onCountrySelected($event: any) {
    this.filterState = $event.name;
  }

  turnOffFilter() {
    this.filterDisplayed = false;
    this.getInitialData();
  }
}
