import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { faWeight, faVenusMars, faHourglass } from '@fortawesome/free-solid-svg-icons';
import {forkJoin, Observable} from "rxjs";


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  userInfo!: any;
  faWeight = faWeight
  faVenusMars = faVenusMars
  faHourGlass = faHourglass
  countryImageSource!: string;

  constructor(private keycloakService: KeycloakService,
    private userService: UserService) { }

  ngOnInit(): void {
    this.getUserInfo();
  }


  async getUserInfo() {
    if (await this.keycloakService.isLoggedIn()) {
      const authDetails = await this.keycloakService.loadUserProfile();
      forkJoin([this.userService.getUser(authDetails.id),this.userService.getUserCompetitionsByUserId(authDetails.id)])
        .subscribe((results: any) => {
        this.userInfo = results[0];
        this.userInfo.userCompetitions = results[1]._embedded.userCompetitions;
        console.log(this.userInfo)
      });
    }
  }
}
