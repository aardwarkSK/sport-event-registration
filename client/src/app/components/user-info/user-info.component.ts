import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit, OnDestroy {
  private routeSub!: Subscription;
  user!: User;


  constructor(private userService: UserService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getUserFromRoute();
  }
  ngOnDestroy(): void {
    this.routeSub.unsubscribe();

  }

  getUserFromRoute() {
    this.routeSub = this.route.params.subscribe(params => {
      const userId = params["id"];
      if (userId) {
        this.userService.getUser(userId).subscribe(result => {
          this.user = result;
        })
      }
    });
  }

}
