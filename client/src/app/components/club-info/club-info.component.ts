import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';
import { ConfirmationService } from 'primeng/api';
import { Subscription } from 'rxjs';
import { Club } from 'src/app/models/club.model';
import { ClubService } from 'src/app/services/club.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-club-info',
  templateUrl: './club-info.component.html',
  styleUrls: ['./club-info.component.scss'],
  providers: [ConfirmationService]
})
export class ClubInfoComponent implements OnInit, OnDestroy {
  private routeSub!: Subscription;
  club!: Club;
  userId!: string | undefined;
  isMember: boolean = false;
  isOwner: boolean = false;


  constructor(private clubService: ClubService,
    private route: ActivatedRoute,
    private keycloakService: KeycloakService,
    private confirmationService: ConfirmationService,
    private userService: UserService) { }

  ngOnInit(): void {
    this.getClubFromRoute();
  }
  ngOnDestroy(): void {
    this.routeSub.unsubscribe();

  }

  getClubFromRoute() {
    this.routeSub = this.route.params.subscribe(params => {
      const clubId = params["id"];
      if (clubId) {
        this.clubService.getClub(clubId).subscribe(result => {
          this.club = result;
          this.getUser();
        })
      }
    });
  }

  async getUser() {
    if (await this.keycloakService.isLoggedIn()) {
      const user = await this.keycloakService.loadUserProfile();
      this.userId = user.id;
      this.getIsUserRelationshipsToClub(user.id);
    }
  }
  getIsUserRelationshipsToClub(id: any) {
    if (this.club._embedded.members) {
      this.isMember = this.club._embedded.members.filter((m: any) => m.id === id).length > 0;
    } else {
      this.isMember = false;
    }
    if (this.club._embedded.owner) {
      this.isOwner = this.club._embedded.owner.id === id;
    }
  }

  editClub() {

  }

  signUpForClub() {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to sign up for this club?',
      accept: () => {
        this.userService.patchUser(this.userId, { club: this.club._links.self.href }).subscribe(r => {
          this.getClubFromRoute();

        });
      }
    });
  }

  leaveClub() {
    this.confirmationService.confirm({
      message: 'Are you sure that you want leave this club?',
      accept: () => {
        this.userService.patchUser(this.userId, { club: '' }).subscribe(r => {
          this.getClubFromRoute();

        });
      }
    });
  }
}
