package com.example.CompetitionSystem.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Competition  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;
    private String name;
    private String about;
    private String mail;
    private String phone;
    private Boolean isInternational;
    private Date dateOfCompetition;
    private Date dateAdded;
    private boolean isFinished;
    @ManyToOne
    @JoinColumn(name="organizer_id")
    private AppUser organizer;
    @OneToOne(cascade = {CascadeType.ALL})
    @JsonIgnoreProperties("competition")
    @JoinColumn(name = "address_id")
    private Adress competitionAddress;
    @OneToMany(mappedBy = "competition")
    List<UserCompetition> userCompetitions;

    public Integer getNumberOfMembers() {
        return userCompetitions.size();
    }
}
