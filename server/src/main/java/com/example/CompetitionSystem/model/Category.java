package com.example.CompetitionSystem.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;
    private Integer weightUnder;
    private Integer ageUnder;
    @ManyToOne(cascade = CascadeType.MERGE,fetch=FetchType.EAGER)
    @JoinColumn(name = "sex_id")
    private Sex sex;

}
