package com.example.CompetitionSystem.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppUser implements Serializable {
    @Id
    private String id;
    private String firstName;
    private String lastName;
    @Column(unique = true, nullable = false)
    private String email;
    private String imgUrl;
    private String belt;
    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="category_id")
    private Category category;
    @OneToOne(cascade = {CascadeType.ALL},orphanRemoval = true)
    @JsonIgnoreProperties("user")
    @JoinColumn(name = "address_id")
    private Adress userAddress;
    @ManyToOne()
    @JoinColumn(name="club_id")
    private Club club;
    @OneToMany(mappedBy = "organizer",orphanRemoval = true)
    private List<Competition> organizedCompetitions;
    @JsonIgnoreProperties("owner")
    @OneToOne(mappedBy = "owner",orphanRemoval = true)
    private Club ownedClub;
    @OneToMany(mappedBy = "user",cascade = CascadeType.ALL,orphanRemoval = true)
    List<UserCompetition> userCompetitions;

    public long getNumberOfWonCompetitions(){
        return this.userCompetitions.stream()
                .filter(u -> u.getPlaced() > 0 && u.getPlaced() < 4).count();
    }

    public Integer getNumberOfCompetedCompetitions(){
        return this.userCompetitions.size();
    }

    public long getRatioOfWinLoss(){
        long won = this.userCompetitions.stream().filter(u -> u.getPlaced() > 0 && u.getPlaced() < 4).count();
        long lost = this.userCompetitions.stream().filter(u -> u.getPlaced() > 3).count();
        return won-lost;

    }
}
