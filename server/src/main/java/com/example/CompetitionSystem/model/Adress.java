package com.example.CompetitionSystem.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Adress implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String street;
    @Column(nullable = false)
    private String city;
    @Column(nullable = false)
    private String state;
    @Column(nullable = false)
    private String zipCode;
    @Column(nullable = false)
    private String alpha2Code;
    @OneToOne(mappedBy = "clubAddress")
    @JsonIgnoreProperties("clubAddress")
    private Club club;
    @OneToOne(mappedBy = "competitionAddress")
    @JsonIgnoreProperties("competitionAddress")
    private Competition competition;


}
