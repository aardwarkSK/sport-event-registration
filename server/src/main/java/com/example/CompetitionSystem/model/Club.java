package com.example.CompetitionSystem.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Club implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;
    private String name;
    private String about;
    private String mail;
    private String phone;
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "address_id")
    private Adress clubAddress;
    @JoinColumn(name = "owner_id")
    @JsonIgnoreProperties("ownedClub")
    @OneToOne(cascade = {CascadeType.ALL})
    private AppUser owner;
    @OneToMany(mappedBy = "club")
    private List<AppUser> members;

    public Integer getNumberOfMembers() {
        return members.size();
    }
}
