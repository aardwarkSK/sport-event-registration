package com.example.CompetitionSystem.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserCompetition implements Serializable {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(nullable = false, updatable = false)
        private Long id;

        @ManyToOne(cascade = {CascadeType.ALL})
        @JoinColumn(name = "user_id")
        private AppUser user;

        @ManyToOne(cascade = {CascadeType.ALL})
        @JoinColumn(name = "competition_id")
        private Competition competition;

        private Integer placed;
        @OneToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="category_id")
        private Category category;
}
