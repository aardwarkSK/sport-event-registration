package com.example.CompetitionSystem.repository;


import com.example.CompetitionSystem.model.AppUser;
import com.example.CompetitionSystem.projection.UserProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "users", path = "users",excerptProjection = UserProjection.class)
public interface AppUserRepo extends JpaRepository<AppUser,String> {
    List<AppUser> findByUserAddressStateIgnoreCaseContainingAndFirstNameIgnoreCaseContainingAndLastNameIgnoreCaseContaining(String state, String name, String surname);
}
