package com.example.CompetitionSystem.repository;

import com.example.CompetitionSystem.model.Competition;
import com.example.CompetitionSystem.projection.CompetitionProjection;
import com.example.CompetitionSystem.projection.UserProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "competitions", path = "competitions",excerptProjection = CompetitionProjection.class)
public interface CompetitionRepo extends JpaRepository<Competition,Long> {
    Optional<Competition> findCompetitionById(Long id);
    List<Competition> findCompetitionByIsFinished(@Param("finished") Boolean finished);
    List<Competition> findByCompetitionAddressStateIgnoreCaseContainingAndNameIgnoreCaseContainingAndIsFinished(String state, String name, Boolean finished);



    void deleteCompetitionById(Long id);
}
