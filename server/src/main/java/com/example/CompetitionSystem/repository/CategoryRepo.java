package com.example.CompetitionSystem.repository;

import com.example.CompetitionSystem.model.Category;
import com.example.CompetitionSystem.projection.UserProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "category", path = "category")
public interface CategoryRepo extends JpaRepository<Category,Long> {
}
