package com.example.CompetitionSystem.repository;

import com.example.CompetitionSystem.model.UserCompetition;
import com.example.CompetitionSystem.projection.CompetitionProjection;
import com.example.CompetitionSystem.projection.UserCompetitionProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "*")
@RepositoryRestResource(collectionResourceRel = "userCompetitions", path = "userCompetitions",excerptProjection = UserCompetitionProjection.class)
public interface UserCompetitionRepo  extends JpaRepository<UserCompetition,Long> {

}
