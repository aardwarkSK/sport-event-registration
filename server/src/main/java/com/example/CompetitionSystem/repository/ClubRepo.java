package com.example.CompetitionSystem.repository;
import com.example.CompetitionSystem.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "clubs", path = "clubs")
public interface ClubRepo extends JpaRepository<Club,Long> {

    List<Club> findByClubAddressStateIgnoreCaseContainingAndNameIgnoreCaseContaining(String state, String name);
}
