package com.example.CompetitionSystem.projection;

import com.example.CompetitionSystem.model.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Formula;
import org.springframework.data.rest.core.config.Projection;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Projection(name = "inlineData", types= Competition.class)
public interface CompetitionProjection {
     Long getId() ;

     String getName() ;

     String getAbout() ;

//     String getMail() ;

//     String getPhone() ;

     Boolean getIsInternational() ;

     Date getDateOfCompetition() ;

     Date getDateAdded() ;

     boolean getIsFinished() ;

     AppUser getOrganizer() ;

     Adress getCompetitionAddress() ;

//     List<UserCompetition> getUserCompetitions();

     Integer getNumberOfMembers() ;

}