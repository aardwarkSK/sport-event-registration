package com.example.CompetitionSystem.projection;

import com.example.CompetitionSystem.model.*;
import org.hibernate.annotations.Formula;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;

@Projection(name = "inlineData", types = AppUser.class)
public interface UserProjection {
    String getId();

    String getFirstName();

    String getLastName();

//    String getEmail();

    String getImgUrl();

    String getBelt();

    Category getCategory();

    Adress getUserAddress();

//    Club getClub();

 //   List<UserCompetition> getUserCompetitions();

//    List<Competition> getOrganizedCompetitions();

//    Club getOwnedClub();

    Integer getNumberOfWonCompetitions();

    Integer getNumberOfCompetedCompetitions();

    Integer getRatioOfWinLoss();
}