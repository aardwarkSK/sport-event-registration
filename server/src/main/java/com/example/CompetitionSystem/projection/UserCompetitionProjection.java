package com.example.CompetitionSystem.projection;

import com.example.CompetitionSystem.model.*;
import org.springframework.data.rest.core.config.Projection;

import java.sql.Date;
import java.util.List;

@Projection(name = "inlineData", types= Competition.class)
public interface UserCompetitionProjection {
     Long getId() ;

     AppUser getUser() ;

     Competition getCompetition() ;

     Integer getPlaced() ;

     Category getCategory();
}
