package com.example.CompetitionSystem.projection;

import com.example.CompetitionSystem.model.*;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;

@Projection(name = "details", types = AppUser.class)
public interface UserDetailsProjection {
    String getId();

    String getFirstName();

    String getLastName();

    String getEmail();

    String getImgUrl();

    String getBelt();

    Category getCategory();

    Adress getUserAddress();

    Club getClub();

    List<UserCompetition> getUserCompetitions();

    List<Competition> getOrganizedCompetitions();

    Club getOwnedClub();
}
