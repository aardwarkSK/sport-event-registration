package com.example.CompetitionSystem.config;

import com.example.CompetitionSystem.model.*;
import com.example.CompetitionSystem.projection.CompetitionProjection;
import com.example.CompetitionSystem.projection.UserCompetitionProjection;
import com.example.CompetitionSystem.projection.UserDetailsProjection;
import com.example.CompetitionSystem.projection.UserProjection;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

@Configuration
public class RestConfig implements RepositoryRestConfigurer {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config, CorsRegistry cors) {
        config.exposeIdsFor(AppUser.class)
                .exposeIdsFor(Competition.class)
                .exposeIdsFor(Club.class)
                .exposeIdsFor(Sex.class)
                .exposeIdsFor(Category.class)
                .exposeIdsFor(UserCompetition.class);
        config.getProjectionConfiguration()
                .addProjection(CompetitionProjection.class)
                .addProjection(UserProjection.class)
                .addProjection(UserCompetitionProjection.class)
                .addProjection(UserDetailsProjection.class);
        RepositoryRestConfigurer.super.configureRepositoryRestConfiguration(config, cors);
    }
}
